import { domain } from "../constants"
export default function getDataApi() {
    return new Promise((resolve, reject) => {
      const url = domain + '/tasks'
      fetch(url, {
        method: "GET"
      })
        .then((response) => response.json())
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          // console.log('error', error)
          reject(error);
        });
    });
  }
  
