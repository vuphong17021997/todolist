import React, { Component } from 'react';
import {
    View, Text, Button, ScrollView,
    TouchableOpacity, ClippingRectangle, Alert, StyleSheet, Modal, TouchableHighlight,
    TouchableWithoutFeedback, Keyboard,
} from "react-native";
import { MaterialIcons } from '@expo/vector-icons';
import { CheckBox } from 'react-native-elements';
import { TextInput } from 'react-native-gesture-handler';
import { FontAwesome } from '@expo/vector-icons';
import SortableList from 'react-native-sortable-list'

class ToDoList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            checked: false,
            task: '',
            modalVisible: false,

        };
    }

    postItem(content) {
        const dataConvert = {
            task: content,
            status: false
        }
        if (content != "") {
            this.props.add(dataConvert)
        } else {
            alert('Chưa thêm task công việc')
        }
        ;
        this.setState({
            task: '',
        })
    }

    changeRow = (key, listOrder) => {
        let tasks = this.props.todos
        // console.warn("L,",listOrder)
        for(let i = 0; i < tasks.length; i++){
            let payload = {
                id: tasks[listOrder[i]].id, 
                order: tasks[i].order 
            }
            if(i === tasks.length - 1) {
                this.props.updateAndGetData(payload)
                break
            }
            this.props.updateDatas(payload)
        }
    }
    
    render() {
        let listTask = [];
        if (this.props.todos) {
            listTask = this.props.todos.map((item, key) => {
                return (
                    <Item key={key} item={item}  {...this.props} />
                )
            })
        }

        return (
            <View style={{ flex: 1, backgroundColor: '#d88771', }} >
                <View
                    style={{
                        flexDirection: 'row',
                        marginTop: 10, paddingHorizontal: 5,
                    }}>
                    <TouchableWithoutFeedback onPress={Keyboard.dismiss} >
                        <TextInput
                            style={{ flex: 1, padding: 10, height: 40, backgroundColor: '#FFFFFF', borderColor: '#BED927', borderRadius: 20, }}
                            underlineColorAndroid="transparent"
                            placeholder="Thêm task công việc..."
                            onChangeText={(task) => this.setState({ task })}
                            value={this.state.task} />
                    </TouchableWithoutFeedback>
                    <TouchableOpacity style={{
                        paddingLeft: 3,
                        marginLeft: -40,
                        width: 40,
                        alignItems: 'center',
                        backgroundColor: '#FF9966',
                        justifyContent: 'center',
                        height: 40,
                        borderTopRightRadius: 5,
                        borderBottomRightRadius: 5
                    }}
                        onPress={() => {
                            Keyboard.dismiss()
                            this.postItem(this.state.task)
                        }}
                    >
                        <MaterialIcons name="add" size={29} color="#ffff" />
                    </TouchableOpacity>
                </View>
                {/* <ScrollView style={{ flex: 1 }}>
                    {listTask}
                </ScrollView> */}
                <SortableList
                    style={{ flex: 1 }}
                    contentContainerStyle={{ flex: 1 }}
                    data={Object.assign({}, this.props.todos)}
                    renderRow={({ data, active }) => (<Item item={data}  {...this.props} />)}
                    // onChangeOrder={nextOrder => { console.warn(nextOrder) }}
                    onReleaseRow={(key, currentOrder) => this.changeRow(key,currentOrder)}
                />  

            </View>

        );
    }
}
class Item extends Component {

    state = { checked: false, id: '', modalVisible: false }

    setModalVisible = (visible) => {
        this.setState({ modalVisible: visible });
    }
    updateChecked = () => {
        const { id } = this.props.item
        this.props.updateDatas({ id, status: !this.state.checked })
        this.setState({ id, checked: !this.state.checked })
    }




    render() {
        const { item } = this.props



        return (
            <View style={{ flex: 1, flexDirection: 'row', marginTop: 10, borderBottomColor: '#EDF8E7', borderBottomWidth: 3, }}>

                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        Alert.alert("Modal has been closed.");
                    }}
                >
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                            <Text style={styles.modalText}>Bạn có chắc muốn xóa !!!</Text>

                            <TouchableHighlight
                                style={{ ...styles.openButton, backgroundColor: "#F4AD0B", margin: 10 }}
                                onPress={() => {
                                    this.setModalVisible(!this.state.modalVisible);
                                }}
                            >
                                <Text style={styles.textStyle}>Cancel </Text>
                            </TouchableHighlight>
                            <TouchableHighlight
                                style={{ ...styles.openButton, backgroundColor: "#2196F3", padding: 12 }}
                                onPress={() => {
                                    this.setModalVisible(!this.state.modalVisible)
                                    this.props.delete(item.id)
                                        ;
                                }}
                            >
                                <Text style={styles.textStyle}>delete</Text>
                            </TouchableHighlight>
                        </View>
                    </View>
                </Modal>
                <View style={{
                    width: '15%', marginTop: -15, marginLeft: -10, marginBottom: -20
                }}>
                    <CheckBox
                        size={40}
                        uncheckedColor='#0DB8B0'
                        checkedColor='#0DB8B0'
                        checked={item.status}
                        onPress={() => this.updateChecked()}
                    />
                </View>

                <View
                    style={{ width: '65%', height: 'auto', padding: 5, alignItems: 'center', justifyContent: 'center' }}>

                    <Text

                        style={{
                            fontSize: 17, fontWeight: '600',
                            textDecorationLine: item.status ? "line-through" : "none",
                            color: item.status ? "gray" : "black"
                        }}>
                        {item.task}
                    </Text>

                </View>
                <View style={{ height: 'auto', borderColor: "#F76634", }}>
                    <TouchableHighlight
                        style={styles.openButton}
                        style={{ marginLeft: 38 }}
                        onPress={() => {
                            this.setModalVisible(true);
                        }}
                    >
                        <Text style={styles.textStyle}> <FontAwesome name="trash-o" size={30} color="#FD2918" /></Text>
                    </TouchableHighlight>


                </View>
            </View >

        )
    }
}
const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    openButton: {
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        textAlign: "center",
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    }
});

export default ToDoList
