import React, { Component } from 'react';
import { connect } from 'react-redux'
import { View } from "react-native";
import ToDoList from '../components/TodoList'
import * as actions from '../actions/ToDoListScreenActions'
class ToDoListContainer extends Component {
    componentDidMount() {
        this.props.initLoad()
    }
    render() {
        return (
            <View style={{ flex: 1, alignSelf: 'stretch', }} >
                <ToDoList {...this.props} />
            </View>
        );
    }
}
//mapStateToProp
const mapStateToProps = (state) => {
    return {
        todos: state.todos.listToDo
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        initLoad: () => {
            dispatch(actions.getData())
        },
        delete: (id) => {
            dispatch(actions.deleteData(id))
        },
        add: (data) => {
            dispatch(actions.addData(data))
        },
        updateDatas: (value) => {
            dispatch(actions.updateData(value))
        },
        updateAndGetData: (value) => {
            dispatch(actions.updateAndGetData(value))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ToDoListContainer)
